class ChangeTrackingStatusType < ActiveRecord::Migration[6.1]
  def change
	change_column :trackings, :status, :integer
  end
end
