class AddUniqueIndexTrackingNumberAndTypeOnTrackings < ActiveRecord::Migration[6.1]
  def change
	add_index :trackings, [:tracking_number, :type], unique: true
  end
end
