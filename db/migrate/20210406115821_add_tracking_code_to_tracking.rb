class AddTrackingCodeToTracking < ActiveRecord::Migration[6.1]
  def change
    add_column :trackings, :tracking_code, :string
    add_index  :trackings, [:tracking_code], unique: true
  end
end
