class CreateTrackings < ActiveRecord::Migration[6.1]
  def change
    create_table :trackings do |t|
      t.string :tracking_number
      t.string :status

      t.timestamps
    end
  end
end
