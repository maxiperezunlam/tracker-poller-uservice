class AddDescriptionToTrackings < ActiveRecord::Migration[6.1]
  def change
    add_column :trackings, :description, :text
  end
end
