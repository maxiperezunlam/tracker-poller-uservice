class AddTypeToTrackings < ActiveRecord::Migration[6.1]
  def change
    add_column :trackings, :type, :string
  end
end
