# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

carriers = %w{ fedex }
carriers.each do |carrier_name|
	Carrier.create(name: carrier_name)
end

# Test data provided by fedex provider
FedexTracking.create(tracking_number: "449044304137821", tracking_code: SecureRandom.uuid)
FedexTracking.create(tracking_number: "149331877648230", tracking_code: SecureRandom.uuid)
FedexTracking.create(tracking_number: "020207021381215", tracking_code: SecureRandom.uuid)
FedexTracking.create(tracking_number: "403934084723025", tracking_code: SecureRandom.uuid)
FedexTracking.create(tracking_number: "920241085725456", tracking_code: SecureRandom.uuid)
FedexTracking.create(tracking_number: "568838414941", tracking_code: SecureRandom.uuid)
FedexTracking.create(tracking_number: "039813852990618", tracking_code: SecureRandom.uuid)
FedexTracking.create(tracking_number: "231300687629630", tracking_code: SecureRandom.uuid)
FedexTracking.create(tracking_number: "797806677146", tracking_code: SecureRandom.uuid)
FedexTracking.create(tracking_number: "377101283611590", tracking_code: SecureRandom.uuid)
FedexTracking.create(tracking_number: "852426136339225", tracking_code: SecureRandom.uuid)
FedexTracking.create(tracking_number: "797615467620", tracking_code: SecureRandom.uuid)
FedexTracking.create(tracking_number: "957794015041323", tracking_code: SecureRandom.uuid)
FedexTracking.create(tracking_number: "076288115212522", tracking_code: SecureRandom.uuid)
FedexTracking.create(tracking_number: "581190049992", tracking_code: SecureRandom.uuid)
FedexTracking.create(tracking_number: "122816215025810", tracking_code: SecureRandom.uuid)
FedexTracking.create(tracking_number: "843119172384577", tracking_code: SecureRandom.uuid)
FedexTracking.create(tracking_number: "070358180009382", tracking_code: SecureRandom.uuid)
FedexTracking.create(tracking_number: "713062653486", tracking_code: SecureRandom.uuid)
