require "test_helper"

class TrackingTest < ActiveSupport::TestCase
  include FactoryBot::Syntax::Methods

  def setup
	Rails.application.load_seed
  end

  def create_policy(tracking_number, carrier)
	TrackingPolicy::Create.new(tracking_number, carrier)
  end

  def update_status_policy(tracking)
        TrackingPolicy::UpdateStatus.new(tracking)
  end

  test "raise exception if tracking is nil when create a TrackingPolicy" do
  	assert_raise(ArgumentError) { create_policy(nil) }
  end

  test "check presence validation if tracking number is nil" do
     policy = create_policy(nil, "fedex")
     assert policy.invalid?
     
     error_message = policy.errors.find { |error| error.message == I18n.t("errors.messages.blank") }
     assert error_message.present?
  end

  test "check presence validation if tracking number is a empty string" do
     policy = create_policy("", "fedex")
     assert policy.invalid?
     
     error_message = policy.errors.find { |error| error.message == I18n.t("errors.messages.blank") }
     assert error_message.present?

     policy = create_policy("         ", "fedex")
     assert policy.invalid?
     error_message = policy.errors.find { |error| error.message == I18n.t("errors.messages.blank") }
     assert error_message.present?
  end

  test "check format validation on tracking number attribute" do
     invalid_tracking = build(:fedex_tracking, :invalid_tracking_number_format)
     policy = create_policy(invalid_tracking.tracking_number, invalid_tracking.type)
     assert policy.invalid?
     
     translation_key = "#{TrackingNumberValidator::I18N_PATH}.format"
     error_message = policy.errors.find { |error| error.message == I18n.t(translation_key) }
     assert error_message.present?
  end

  test "check that policy test returns false with a existing tracking" do
     tracking = create(:fedex_tracking, :delivered)
     policy = create_policy(tracking.tracking_number, 'fedex')
     assert policy.invalid?

     translation_key = "#{TrackingUniquenessValidator::I18N_PATH}.uniqueness"
     error_message = policy.errors.find { |error| error.message == I18n.t(translation_key) }
     assert error_message.present?
  end 
  
  test "check that policy test returns true with a valid tracking object" do 
     valid_tracking = build(:fedex_tracking, :recorded)
     policy = create_policy(valid_tracking.tracking_number, "fedex")
     assert policy.valid?, "#{policy.errors.full_messages.join('\n')}"
  end

  test "check presence validation on status attribute when user want update it" do
     tracking_status = nil
     policy = update_status_policy(tracking_status)
     assert policy.invalid? 
     
     error_message = policy.errors.find { |error| error.message == I18n.t("errors.messages.blank") }
     assert error_message.present?
  end

  test "check format validation with invalid status attribute" do
     tracking_status = Faker::String.random
     policy = update_status_policy(tracking_status)
     assert policy.invalid?
 
     translation_key = "#{TrackingStatusValidator::I18N_PATH}.format"
     error_message = policy.errors.find { |error| error.message == I18n.t(translation_key) }
     assert error_message.present?
  end

  test "check if validator returns an error if tracking does not exists on database" do 
     tracking_code = Faker::String.random
     policy = TrackingPersistedValidator.new(tracking_code)
     assert policy.invalid?

     translation_key = "#{TrackingPersistedValidator::I18N_PATH}.not_found"
     error_message = policy.errors.find { |error| error.message == I18n.t(translation_key) }
     assert error_message.present?
  end
end
