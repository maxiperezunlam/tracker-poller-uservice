FactoryBot.define do
  factory :tracking do
    trait :recorded do
      tracking_number { Faker::Number.leading_zero_number(digits: 15) }
      tracking_code { SecureRandom.uuid }
    end

    trait :created do
      tracking_number { Faker::Number.leading_zero_number(digits: 15)  }
      status { TrackerShip::TrackingState::Created.formatted_s.downcase }
      tracking_code { SecureRandom.uuid }
      created_at { DateTime.now }
      updated_at { DateTime.now }
    end

    trait :on_transit do
      tracking_number { Faker::Number.leading_zero_number(digits: 15) }
      status { TrackerShip::TrackingState::OnTransit.formatted_s.downcase }
      tracking_code { SecureRandom.uuid }
      created_at { 1.week.ago }
      updated_at { 1.day.ago } 
    end

    trait :delivered do
      tracking_number { Faker::Number.leading_zero_number(digits: 15) }
      status { TrackerShip::TrackingState::Delivered.formatted_s.downcase }
      tracking_code { SecureRandom.uuid }
      created_at { 3.week.ago }
      updated_at { DateTime.now }
    end

    trait :exception do
	tracking_number { Faker::Number.leading_zero_number(digits: 15) }
	status { TrackerShip::TrackingState::Exception.formatted_s.downcase }
        tracking_code { SecureRandom.uuid }
	created_at { 2.week.ago }
	updated_at { 4.day.ago } 
    end

    trait :invalid_tracking_number_format do
    	tracking_number { Faker::Number.leading_zero_number(digits: Faker::Number.between(from: 1, to: 14)) }
    end

    trait :invalid_status_format do
        tracking_number { Faker::Number.leading_zero_number(digits: 15) }
        status { Faker::String.random }
    end

    trait :invalid_tracking_code_format do
	tracking_number { Faker::Number.leading_zero_number(digits: 15) }
	tracking_code { Faker::String.random }
    end
  end

  factory :fedex_tracking, parent: :tracking, class: FedexTracking do
  	type { "FedexTracking" }
  end
end
