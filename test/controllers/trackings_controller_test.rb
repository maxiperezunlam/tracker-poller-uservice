require "test_helper"

class TrackingsControllerTest < ActionDispatch::IntegrationTest
  include FactoryBot::Syntax::Methods
  AUTH_TOKEN = Rails.application.credentials.api[:token]

  def setup
    Rails.application.load_seed
  end
  
  # #show action
  test "#show get bad request response because tracking code is nil from /v1/trackings/:tracking_code" do
  	get v1_tracking_url(" "), headers: { "HTTP_AUTHORIZATION" => %{Token token="#{AUTH_TOKEN}"} } 
	assert_equal 400, status

	body = response.parsed_body
	assert_equal(response_entity(body), entity.to_s.underscore)
	assert_equal(error_attribute(body), 'tracking_code')
	assert_equal(error_type(body), 'blank')
  end


  test "#show get bad request response for tracking code format error from /v1/trackings/:tracking_code" do
  	get v1_tracking_url(Faker::String.random(length: 15)), headers: { "HTTP_AUTHORIZATION" => %{Token token="#{AUTH_TOKEN}"} } 

	assert_equal 400, status
	
	body = response.parsed_body
	
	assert_equal(response_entity(body), entity.to_s.underscore)
	assert_equal(error_attribute(body), 'tracking_code')
	assert_equal(error_type(body), 'invalid')
  end

  test "#show get tracking not found response from /v1/trackings/:tracking_code" do
  	get v1_tracking_url(SecureRandom.uuid), headers: { "HTTP_AUTHORIZATION" => %{Token token="#{AUTH_TOKEN}"} } 

        assert_equal 400, status	
	
	body = response.parsed_body
	assert_equal(response_entity(body), entity.to_s.underscore)
	assert_equal(error_attribute(body), 'tracking_code')
	assert_equal(error_type(body), 'not_found')
  end

  test "#show get successfully a tracking delivery status information from /v1/trackings/:tracking_number" do
     tracking = create(:fedex_tracking, :delivered)
     get v1_tracking_url(tracking.tracking_code), headers: { "HTTP_AUTHORIZATION" => %{Token token="#{AUTH_TOKEN}"} } 

     assert_equal 200, status
     
     body = response.parsed_body
     assert_equal(response_entity(body), entity.to_s.underscore)
     #Chequeamos que el tracking number corresponda con el tracking number enviado
     assert_equal(body['data']['tracking_number'], tracking.tracking_number)
     #Chequeamos que el estado del paquete sea el esperado
     assert_equal(delivery_status(body), TrackerShip::TrackingState::Delivered.formatted_s.downcase)
     #Chequeamos que esten presentes los timestamps
     assert_equal(body['data']['created_at'].present?, true)
     assert_equal(body['data']['updated_at'].present?, true)
     assert_equal(body['data']['id'].blank?, true) #Chequeamos que se evite la fuga de ids internos a la aplicacion
  end

  test "#show get 500 HTTP error when a unexpected error occurs" do
  	tracking = create(:fedex_tracking, :created)
        mock = get_delivery_status_exception_mock

	ApiRequests::Trackings::GetDeliveryStatus.stub(:new, mock) do
		get v1_tracking_url(tracking.tracking_code), headers: { "HTTP_AUTHORIZATION" => %{Token token="#{AUTH_TOKEN}"} } 

		assert_equal 500, status
	end
  end

  # #create action
  test "#create get 500 HTTP error when a unexpected error occurs" do
  	tracking_number = Faker::Number.leading_zero_number(digits: 15)
	carrier = 'fedex'
	mock = get_create_tracking_if_not_exists_exception_mock

	ApiRequests::Trackings::CreateTrackingIfNotExists.stub(:new, mock) do
		post v1_trackings_url, params: { tracking_number: tracking_number, carrier: carrier }, headers: { "HTTP_AUTHORIZATION" => %{Token token="#{AUTH_TOKEN}"} } 
 
		assert_equal 500, status
	end
  end

  test "#create get 400 HTTP error when the format of tracking number is invalid" do
  	tracking_number = Faker::Number.leading_zero_number(digits: 14)
	carrier = 'fedex'
	post v1_trackings_url, params: { tracking_number: tracking_number, carrier: carrier }, headers: { "HTTP_AUTHORIZATION" => %{Token token="#{AUTH_TOKEN}"} } 

	assert 400, status	
	body = response.parsed_body
	assert_equal(response_entity(body), entity.to_s.underscore)
	assert_equal(error_attribute(body), 'tracking_number')
	assert_equal(error_type(body), 'invalid')
  end
 
  test "#create get 400 HTTP error when the carrier is invalid" do
  	tracking_number = Faker::Number.leading_zero_number(digits: 15)
	carrier = 'fake'
	post v1_trackings_url, params: { tracking_number: tracking_number, carrier: carrier }, headers: { "HTTP_AUTHORIZATION" => %{Token token="#{AUTH_TOKEN}"} } 

 
	assert 400, status	
	body = response.parsed_body
	assert_equal(response_entity(body), entity.to_s.underscore)
	assert_equal(error_attribute(body), 'carrier')
	assert_equal(error_type(body), 'invalid')
  end

  test "#create get 400 HTTP error when the tracking already exists" do
	tracking = create(:fedex_tracking, :on_transit)
	post v1_trackings_url, params: { tracking_number: tracking.tracking_number, carrier: 'fedex' }, headers: { "HTTP_AUTHORIZATION" => %{Token token="#{AUTH_TOKEN}"} } 

	assert 409, status	
	body = response.parsed_body
	assert_equal(response_entity(body), entity.to_s.underscore)
	assert_equal(error_attribute(body), 'tracking_number')
	assert_equal(error_type(body), 'uniqueness')
  end

  test "#create get 201 HTTP code when the tracking was created successfully" do
  	post v1_trackings_url, params: { 
					  tracking_number: Faker::Number.leading_zero_number(digits: 15),
					  carrier: 'fedex'
				       }, headers: { "HTTP_AUTHORIZATION" => %{Token token="#{AUTH_TOKEN}"} } 

	assert 201, status
	body = response.parsed_body
	assert_equal(response_entity(body), entity.to_s.underscore)
	assert_equal(body['data']['tracking_code'].present?, true)
	assert_equal(body['data']['description'].present?, true)
  end

  test "get status 200 when check health endpoint" do
  	get v1_health_url
	assert 200, status
  end

  private 

  def entity
	Tracking
  end

  # Por que el ejemplo tiene un unico controller no voy a refactorizar este codigo a un modulo. No obstante 
  # Seria conveniente debido a su caracter de generico dentro del formato de respuestas de error que manejamos en 
  # la aplicacion.
  def response_entity(body)
 	body['entity'] 
  end

  def error_attribute(body, error_number = 0)
  	body['errors'][error_number]['attributes']&.first
  end

  def error_attributes(body, errors_number = 0)
  	body['errors'][error_number]['attributes']
  end

  def error_type(body, error_number = 0)
        body['errors'][error_number]['type']
  end

  def delivery_status(body)
	body['data']['status']
  end

  def get_delivery_status_exception_mock
	mock = Minitest::Mock.new

	def mock.new(tracking_number)	
		raise StandardError, "An stubbed error"
	end

	def mock.call
		raise StandardError, "An stubbed error"
	end

	mock
  end

  def get_create_tracking_if_not_exists_exception_mock
	mock = Minitest::Mock.new

	def mock.new(tracking_number, carrier)
		raise StandardError, "An stubbed error"
	end

	def mock.call
		raise StandardError, "An stubbed error"
	end

	mock
  end
end
