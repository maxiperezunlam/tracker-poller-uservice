require "test_helper"

class HealthControllerTest < ActionDispatch::IntegrationTest
  test "#health check if health check endpoint is working" do
     get v1_health_url
     assert 200, status
  end
end
