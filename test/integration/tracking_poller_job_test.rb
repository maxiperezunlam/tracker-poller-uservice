require 'test_helper'
require 'dry-monads'
require 'sidekiq'
require 'sidekiq/testing'

require_relative File.join(__dir__, '..', '..', 'lib', 'jobs', 'tracking_poller_job.rb')
require_relative File.join(__dir__, '..', '..', 'lib', 'tracker_ship', 'all')
require_relative File.join(__dir__, '..', '..', 'lib', 'dummy_redis')
TrackerShip::All.load!

class PollerTest < ActionDispatch::IntegrationTest
	include FactoryBot::Syntax::Methods

	def setup
		@status_nil_to_created = create(:fedex_tracking, :recorded)
		@created_to_on_transit = create(:fedex_tracking, :created)
		@on_transit_to_delivered = create(:fedex_tracking, :on_transit)
		@delivered_no_change = create(:fedex_tracking, :delivered)
		@exception_on_ship = create(:fedex_tracking, :delivered)
		
		ENV["POLLER_JOB_ENV"] = 'test' #Change the environment
		ENV["POLLER_JOB_DB_CONFIG_FILEPATH"] = '../../config/database.yml'
		ENV["POLLER_JOB_ONLY_JOB_SERVER"] = "false"

		Sidekiq::Testing.fake!
	end

	test "check that job update delivery status on database" do
	        job, poller_mock, dummy_redis_mock = get_data_for_integration_test
		job.poller = TrackerShip::TrackerPoller::Fedex.new nil, -> { [] }

		job.poller.stub(:call, test_result_data) do
			#Usamos un dummy para evitar conexiones con sockets que pueden producir errores al no estar mockeandolas
			job.redis = DummyRedis.new
			job.updated_at = updated_at = Time.now.utc
			job.redis.stub(:publish, -> (channel, message) { [] }) do
				job.perform
				
				tracking = FedexTracking.find_by(tracking_number: @status_nil_to_created.tracking_number)
				assert_equal tracking.status, "created"
				assert_equal tracking.description, "created"
				assert_equal normalize_date(tracking.updated_at), normalize_date(updated_at)
				
				tracking = FedexTracking.find_by(tracking_number: @created_to_on_transit.tracking_number)
				assert_equal tracking.status, "on_transit"
				assert_equal tracking.description, "on_transit"
				assert_equal normalize_date(tracking.updated_at), normalize_date(updated_at)
				
				tracking = FedexTracking.find_by(tracking_number: @on_transit_to_delivered.tracking_number)
				assert_equal tracking.status, "delivered"
				assert_equal tracking.description, "delivered"
				assert_equal normalize_date(tracking.updated_at), normalize_date(updated_at)
				
				tracking = FedexTracking.find_by(tracking_number: @delivered_no_change.tracking_number)
				assert_equal tracking.status, "delivered"
				assert_equal tracking.description, "delivered"
				assert_equal normalize_date(tracking.updated_at), normalize_date(updated_at)
				
				tracking = FedexTracking.find_by(tracking_number: @exception_on_ship.tracking_number)
				assert_equal tracking.status, "exception"
				assert_equal tracking.description, "ship exception"
				assert_equal normalize_date(tracking.updated_at), normalize_date(updated_at)
			end
		end
	end

	test "check that job publish a message on redis channel" do
		job, poller_mock, dummy_redis_mock = get_data_for_integration_test
		job.poller = TrackerShip::TrackerPoller::Fedex.new nil, -> { [] }

		job.poller.stub(:call, test_result_data) do
			job.redis = DummyRedis.new
			job.updated_at = updated_at = Time.now.utc
			job.perform
			
			tracking_json = JSON.parse(job.redis.messages[ENV["TRACKINGS_DELIVERY_STATUS_CHANNEL"]].pop)
			assert_equal tracking_json["tracking_number"], @status_nil_to_created.tracking_number
			assert_equal tracking_json["status"], 0
			assert_equal tracking_json["description"], 'created'
			assert_equal tracking_json["updated_at"].present?, true
			assert_equal normalize_date(tracking_json["updated_at"]), normalize_date(updated_at)
			
			tracking_json = JSON.parse(job.redis.messages[ENV["TRACKINGS_DELIVERY_STATUS_CHANNEL"]].pop)
			assert_equal tracking_json["tracking_number"], @created_to_on_transit.tracking_number
			assert_equal tracking_json["status"], 1
			assert_equal tracking_json["description"], 'on_transit'
			assert_equal tracking_json["updated_at"].present?, true
			assert_equal normalize_date(tracking_json["updated_at"]), normalize_date(updated_at)
			
			tracking_json = JSON.parse(job.redis.messages[ENV["TRACKINGS_DELIVERY_STATUS_CHANNEL"]].pop)
			assert_equal tracking_json["tracking_number"], @on_transit_to_delivered.tracking_number
			assert_equal tracking_json["status"], 2
			assert_equal tracking_json["description"], "delivered"
			assert_equal tracking_json["updated_at"].present?, true
			assert_equal normalize_date(tracking_json["updated_at"]), normalize_date(updated_at)
			
			tracking_json = JSON.parse(job.redis.messages[ENV["TRACKINGS_DELIVERY_STATUS_CHANNEL"]].pop)
			assert_equal tracking_json["tracking_number"], @delivered_no_change.tracking_number
			assert_equal tracking_json["status"], 2
			assert_equal tracking_json["description"], "delivered"
			assert_equal tracking_json["updated_at"].present?, true
			assert_equal normalize_date(tracking_json["updated_at"]), normalize_date(updated_at)
			
			tracking_json = JSON.parse(job.redis.messages[ENV["TRACKINGS_DELIVERY_STATUS_CHANNEL"]].pop)
			assert_equal tracking_json["tracking_number"], @exception_on_ship.tracking_number
			assert_equal tracking_json["status"], 3
			assert_equal tracking_json["description"], "ship exception"
			assert_equal tracking_json["updated_at"].present?, true
			assert_equal normalize_date(tracking_json["updated_at"]), normalize_date(updated_at)
		end
	end

	private

	def get_data_for_integration_test
		job = TrackingPollerJob.new
		tracker_poller_mock = get_poller_mock do |response|
			 response = test_result_data
		end
		
		[job, tracker_poller_mock]
	end

	def test_result_data
	 	[
				TrackerShip::Status::Success.new(@status_nil_to_created.tracking_number, 
								 ::Dry::Monads[:result]::Success.new(
									TrackerShip::TrackingState::Created.new("created")
								 )),
				TrackerShip::Status::Success.new(@created_to_on_transit.tracking_number, 
								 ::Dry::Monads[:result]::Success.new(
									TrackerShip::TrackingState::OnTransit.new("on_transit")
								)),
				TrackerShip::Status::Success.new(@on_transit_to_delivered.tracking_number, 
								 ::Dry::Monads[:result]::Success.new(
									TrackerShip::TrackingState::Delivered.new("delivered")
								)),
				TrackerShip::Status::Success.new(@delivered_no_change.tracking_number, 
								 ::Dry::Monads[:result]::Success.new(
									TrackerShip::TrackingState::Delivered.new("delivered")
								)),
				TrackerShip::Status::Success.new(@exception_on_ship.tracking_number, 
								 ::Dry::Monads[:result]::Failure.new(
									TrackerShip::TrackingState::Exception.new("ship exception")
								))
		]
	end

	def get_poller_mock
		mock = Minitest::Mock.new

		response = nil
		yield(response)
		
		def mock.call
			return response	
		end	

		return mock
	end

	def normalize_date(date)
		if date.is_a?(Time)
			date.strftime('%d-%m-%Y %H:%M')
		elsif date.is_a?(String)
			Time.parse(date).strftime('%d-%m-%Y %H:%M')
		else
			raise :unknown_type
		end
	end
end

