Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :v1, defaults: { format: :json } do
  	post 'trackings' => 'trackings#create', as: :trackings
	get  'trackings/:tracking_code' => 'trackings#show', as: :tracking 
	get  'health' => 'health#health', as: :health
  end
end
