# READMEG

## Ruby version

The ruby version used is 3.0.0

## System dependencies

* ruby 3.0.0 or greater
* redis 4.41 or greater

## Configuration

`.env` file has basic configuration for health checking endpoints. 

## Database creation and initialization

`bin/rails db:create && bin/rails db:migrate && bin/rails db:seed`

## How to run the test suite

Execute `bin/rails test`

## Health Checking

The api has a health checking endpoint on http://domain.com/health. For example on localhost the endpoint is http://localhost:port/health.
This endpoint returns information about the status of:

* API
* Redis
* Database and migrations
* Sidekiq jobs

## Manual Testing

In the root folder of project is included a json file for to be imported into postman. This is a collection with all endpoints mocked for testing. Each request has a description with more information.

## Services (job queues, cache servers, search engines, etc.)

The project use sidekiq and sidekiq-scheduler for recurring jobs. This job (lib/poller.rb) fetch the delivery status data from the registered 
trackings from 3 months ago to the current date. 

## Deployment instructions

- On ubuntu systems run `bin/setup` and the execute foreman command (Item 8). If you run this project on another enviroment:

1. Install ruby 3.0 or greater
2. Install rails 6.0.0 or greater 
3. Install redis-server
4. execute on base project folder `bundle install`
5. execute on base project folder `bin/rails db:create`
6. execute on base project folder `bin/rails db:migrate`
7. execute on base project folder `bin/rails db:seed`
8. install foreman (it is not bundled so because author gem suggestions)
9. execute on base project folder `bin/rails s`
10. execute `redis-server` (if it is not running as a service)
11. execute on base project folder `sidekiq -r ./lib/jobs/tracking_poller_job.rb` 
10. Optional. If you want check if the project works you can run bin/subscriber_example on another terminal session. (redis must be running) 

## Arquitecture 

The architecture is a micro service using a HTTP API (Rails). An external application create trackings to be analyzed using '/v1/trackings' method. This endpoint expect the tracking number and the carrier. The application would receive a tracking code in the response for synchronously to check the delivery status of shipping. Also, a recurring sidekiq job run each X time processing all registered trackings with an updated date greater than 3 months ago. The sidekiq job save the updated delivery status into database and publish a message on 'trackings/delivery_status' redis channel. 

The configuration can be changed modifying .env file mainly. Also the API token is stored using Rails credentials feature. For development and testing the API Token is "1234" ( :D ).    

## TODO

1. Refactor tracker poller: to support streaming operations.
2. Refactor tracker poller: to implement a circuit breaker pattern.
3. Refactor poller sidekiq job: to support clustering processing and load balancing.
4. Add Swagger (or another solution) for api documentation

## Contact Information

Maximiliano Perez, <maxiperezunlam@gmail.com>

You can download my updated CV from this [link](https://bitbucket.org/maxiperezunlam/cv)
