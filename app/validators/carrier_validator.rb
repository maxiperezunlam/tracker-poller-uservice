class CarrierValidator < BaseValidator
	I18N_PATH = "errors.validators.carrier"

	attr_reader :carrier

	def initialize(carrier)
		@carrier = carrier
	end

	validates :carrier, presence: true
	validate :is_valid_carrier?

	private

	def is_valid_carrier?
		if @carrier.present?
			unless carrier.match(valid_carrier_regexp).present?
				errors.add(:carrier, :invalid, message: I18n.t("#{I18N_PATH}.invalid"))
			end
		end
	end

	def valid_carrier_regexp
		%r{\A(#{Carrier.pluck(:name).join("|")})(Tracking)?\z}xi.freeze
	end
end
