# frozen_string_literal: true

class TrackingUniquenessValidator < BaseValidator
	I18N_PATH = "errors.validators.trackings"
	
	attr_reader :tracking_number, :carrier

	def initialize(tracking_number, carrier)
		@tracking_number = tracking_number
		@carrier = carrier
	end

	validate :already_exists_tracking?

	private

	def already_exists_tracking?
		carrier_klass = Tracking.class_for(@carrier)

		@result = carrier_klass.find_by(tracking_number: @tracking_number)

		if @result.present?
			errors.add(:tracking_number, :uniqueness, message: I18n.t("#{I18N_PATH}.uniqueness"))
		end
	end
end
