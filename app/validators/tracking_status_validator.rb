# frozen_string_literal: true

class TrackingStatusValidator < BaseValidator
	VALID_STATUS_REGEXP = %r{\A#{ Regexp.new(TrackerShip::TrackingState.valid_states.join('|')) }\z}xi.freeze	
	I18N_PATH = "errors.validators.tracking_status"
	attr_reader :status

	def initialize(status)
		@status = status
	end

	validates :status, presence: true
	validates :status, allow_blank: true, format: {
						        with: VALID_STATUS_REGEXP,
							message: I18n.t("#{I18N_PATH}.format")
						      }
end	
