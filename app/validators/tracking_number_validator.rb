# frozen_string_literal: true

class TrackingNumberValidator < BaseValidator
	TRACKING_NUMBER_FORMAT = %r{\A\d{15}\z}.freeze	
	I18N_PATH = "errors.validators.tracking_number"
	attr_reader :tracking_number

	def initialize(tracking_number)
		@tracking_number = tracking_number
	end

	validates :tracking_number, presence: true
	validates :tracking_number, allow_blank: true, 
				    	    format: { with: TRACKING_NUMBER_FORMAT,
					              message: I18n.t("#{I18N_PATH}.format") }
end
