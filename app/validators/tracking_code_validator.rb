# frozen_string_literal: true

class TrackingCodeValidator < BaseValidator
	TRACKING_NUMBER_FORMAT =  /\A[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}\z/i.freeze	
	I18N_PATH = "errors.validators.tracking_code"
	attr_reader :tracking_code

	def initialize(tracking_code)
		@tracking_code = tracking_code
	end

	validates :tracking_code, presence: true
	validates :tracking_code, allow_blank: true, 
				    	    format: { with: TRACKING_NUMBER_FORMAT,
					              message: I18n.t("#{I18N_PATH}.format") }
end
