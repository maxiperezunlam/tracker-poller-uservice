# frozen_string_literal: true

class TrackingPersistedValidator < BaseValidator
	I18N_PATH = "errors.validators.tracking_code"

	attr_reader :tracking_code, :tracking

	def initialize(tracking_code, repository = Tracking)
		@repository = repository
		@tracking_code = tracking_code
	end

	validate :is_tracking_persisted?

	private

	def is_tracking_persisted?
		@tracking = @repository.find_by(tracking_code: tracking_code) if tracking_code.present?
		errors.add(:tracking_code, :not_found, message: I18n.t("#{I18N_PATH}.not_found")) unless @tracking.present?
	end
end
