# frozen_string_literal: true

class BaseValidator
	include ActiveModel::Validations

	attr_reader :result
end
