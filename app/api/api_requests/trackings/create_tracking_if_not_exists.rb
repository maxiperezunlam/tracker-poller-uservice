module ApiRequests
	module Trackings
		class CreateTrackingIfNotExists
			attr_reader :tracking_number, :carrier

			def initialize(tracking_number, carrier, 
				       create_policy = TrackingPolicy::Create)
				@tracking_number = tracking_number
				@carrier = carrier
				@create_policy = create_policy
			end

			def call
				policy = @create_policy.new tracking_number, carrier

				unless policy.valid?
					if policy.fail_by?(:invalid_tracking_number) || policy.fail_by?(:invalid_carrier)		
						return ApiResponses::Trackings::CreateBadRequest.new(policy.errors)	
					end

					if policy.fail_by?(:already_exists)
						tracking_code = policy.results[:tracking].tracking_code
						return ApiResponses::Trackings::AlreadyExist.new(tracking_code) 
					end
				end

				return ApiResponses::Trackings::Created.new(create_tracking!(tracking_number, carrier))
			end

			private

			def create_tracking!(tracking_number, carrier)
				Tracking.class_for(carrier).create(tracking_number: tracking_number, tracking_code: SecureRandom.uuid)
			end
		end
	end
end
