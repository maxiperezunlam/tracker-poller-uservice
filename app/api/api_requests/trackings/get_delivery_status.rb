# frozen_string_literal: true

module ApiRequests
	module Trackings
		class GetDeliveryStatus < ApiRequests::BaseRequest
			attr_reader :tracking_code
			attr_accessor :format_validator_klass, :persisted_validator_klass

			def initialize(tracking_code, 
				       format_validator_klass = TrackingCodeValidator,
				       persisted_validator_klass = TrackingPersistedValidator)
				@tracking_code = tracking_code
				@format_validator_klass = format_validator_klass
				@persisted_validator_klass = persisted_validator_klass
			end

			def call
				format_validator = format_validator_klass.new(tracking_code)
				return ApiResponses::Trackings::BadTrackingCodeFormat.new(
					             format_validator.errors
						     ) unless format_validator.valid?

				persisted_validator = persisted_validator_klass.new(tracking_code)
				return ApiResponses::Trackings::NotEntityFound.new(
						     persisted_validator.errors
						     ) unless persisted_validator.valid?  

				# En pos de tener una API  de validaciones unificada, rendimiento y 
				# simplificacion se opto por que ciertas clases de validacion que 
				# tienen que buscar en la base de datos para poder verificar por ejemplo 
				# la existencia de la entidad o que un atributo sea unico, retornen
				# la entidad buscada. Otra opciones factibles son:
				# 	* refactorizar la logica de validacion a un metodo privado del request object
				#	* Tener QueryObjects (ActiveRecord solo para persistencia) de informacion 
				#	  que ademas cachean el resultado de la query. Por lo que instanciariamos una 
				#	  query object, se lo pasamos como dependencia al validator, dentro del 
				#	  validator se ejecuta la query y se realiza la validacion correspondiente. 
				#	  En caso de ser valido el siguiente paso en el request object es obtener el 
				#	  resultado desde el Query Object pasado como dependencia al validador.
				tracking = persisted_validator.tracking
			        
				return ApiResponses::Trackings::GetDeliveryStatusSuccess.new(tracking)	
			end
		end
	end
end
