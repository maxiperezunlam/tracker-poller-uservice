module ApiResponses
	class BaseResponse < Hash
		def status
			self[:status]
		end

		def entity
			self[:entity]
		end

		protected 

		def initialize(entity, status)
			self[:entity] = entity.to_s.underscore
			self[:status] = status
		end
	end
end
