module ApiResponses
	class SuccessResponse < ApiResponses::BaseResponse
		def data
			self[:data]
		end
	
		protected
		
		def initialize(entity, status, data = nil)
			super(entity, status)
			self[:data] = data.present? ? map_response_data(data) : ''
		end

		# En caso que los datos sean un objeto active record proveemos una implementacion por defecto
		# En caso contrario requerimos a la subclase que sobrescriba el metodo e implemente el mapeo
		# correspondiente. 
		def map_response_data(data)
			return data.attributes.to_hash.except("id") if data.is_a?(ApplicationRecord)

			raise NotImplementedError, "Parameter data is not a ApplicationRecord instance. #{self.class} must implement '#{__method__}' method."
		end
	end
end
