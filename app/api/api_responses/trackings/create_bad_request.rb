module ApiResponses
	module Trackings
		class CreateBadRequest < ApiResponses::ErrorResponse
			def initialize(errors)
				super(Tracking, 400, errors)
			end
		end
	end
end
