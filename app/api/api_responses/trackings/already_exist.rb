module ApiResponses
	module Trackings
		class AlreadyExist < ApiResponses::ErrorResponse
			def initialize(tracking_code)
				super(Tracking, 409, {
					attributes: ['tracking_number', 'carrier'],
					type: 'uniqueness',
					message: "Tracking already was registered. You can request the updated delivery status using '/trackings/#{tracking_code}' endpoint"
				})
				
			end
		end
	end
end
