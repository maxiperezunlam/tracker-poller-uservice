# frozen_string_literal: true

module ApiResponses
	module Trackings
		class GetDeliveryStatusSuccess < ApiResponses::SuccessResponse
			def initialize(entity)
				super(Tracking, 200, entity)
			end
		end
	end
end
