module ApiResponses
	module Trackings
		class Created < ApiResponses::SuccessResponse
			def initialize(tracking)
				super(Tracking, 201, tracking)
			end

			def map_response_data(data)
				self[:data] = {
					tracking_code: data.tracking_code,
					description: "You can query the delivery status using the provided tracking code on '/trackings/:tracking_code' url"
				}
			end
		end
	end
end
