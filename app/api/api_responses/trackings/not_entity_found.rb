# frozen_string_literal: true

module ApiResponses
	module Trackings
		class NotEntityFound < ApiResponses::ErrorResponse
			def initialize(errors)
				super(Tracking, 400, errors)
			end
		end
	end
end
