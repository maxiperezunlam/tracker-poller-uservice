module ApiResponses
	class ErrorResponse < ApiResponses::BaseResponse
		def errors
			self[:errors]
		end

		protected

		def initialize(entity, status, errors)
			super(entity, status)

			if errors.nil? || errors.empty? || !errors.respond_to?(:map)
				self[:errors] = [{
					attributes: [],
					type: "#{self.class.name.split("::").last}Error".underscore,
					message: ""}]
			else
				if errors.is_a?(Hash)
					self[:errors] = [errors]
				else 
					self[:errors] = errors.map do |error|
						{
							attributes: [ error.attribute ],
							type: error.type,
							message: error.full_message
						}
					end
				end
			end
		end
	end
end
