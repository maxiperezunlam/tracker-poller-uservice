# frozen_string_literal: true
class Tracking < ApplicationRecord
	TRACKING_KLASS_REGEXP = /\A.+Tracking\z/

	enum status: TrackerShip::TrackingState.valid_states.map.with_index.to_h.freeze		
	after_initialize :raise_error_on_tracking_initialization
	
	# Esta validacion la agregamos para evitar que alguien pueda setear el valor nulo
	validates :type, presence: true
	validate :is_type_valid?
	validates :type, uniqueness: { scope: :tracking_number }

	def self.class_for(carrier)
		carrier.match(TRACKING_KLASS_REGEXP).present? ? carrier.constantize : "#{carrier.classify}Tracking".constantize
	end

	protected

	def raise_error_on_tracking_initialization
		raise RuntimeError, "Cannot initialize abstract class 'Tracking'" if self.class == Tracking
	end

	def is_type_valid?
		load_child_klasses!
		unless Tracking.descendants.map(&:to_s).include?(self.type)
				errors.add(:type, :invalid, message: 'Invalid Tracking model Subclass')
		end
	end

	def load_child_klasses!
		unless defined? @loaded_child_klasses && @loaded_child_klasses
			require 'fedex_tracking.rb'
			@loaded_child_klasses = true
		end
	end	
end
