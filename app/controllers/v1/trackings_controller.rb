# frozen_string_literal: true

class V1::TrackingsController < ApplicationController
   before_action :authenticate

   def create
  	tracking_number = tracking_params[:tracking_number] 
        carrier = tracking_params[:carrier]

	begin
		request = ApiRequests::Trackings::CreateTrackingIfNotExists.new(tracking_number, carrier)
		response = request.call
		render json: response, status: response.status
	rescue StandardError => e
		Rails.logger.error(e.message)
		render status: 500
	end
   end


   def show
   	tracking_code = tracking_params[:tracking_code]
	
	begin
		request = ApiRequests::Trackings::GetDeliveryStatus.new(tracking_code)
		response = request.call	
		render json: response, status: response.status
	rescue StandardError => e
		Rails.logger.error(e.message)
		render status: 500
	end
   end

   private

   def tracking_params
   	params.permit(:tracking_number, :carrier, :tracking_code)
   end

   def authenticate
	authenticate_or_request_with_http_token do |token, _|
		Rails.application.credentials.api[:token] == token
	end		
   end

   def current_user
     @current_user ||= authenticate
   end
end
