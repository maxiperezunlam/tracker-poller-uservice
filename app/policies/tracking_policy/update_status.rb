module TrackingPolicy
	class UpdateStatus
		include ActiveModel::Validations

		attr_reader :tracking_status

		def initialize(tracking_status)
			@tracking_status = tracking_status
		end

		validate :is_status_valid?

		private

		def is_status_valid?
			validator = TrackingStatusValidator.new(tracking_status)
			errors.merge!(validator.errors) unless validator.valid?
		end
	end
end
