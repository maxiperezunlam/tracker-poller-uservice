# frozen_string_literal: true

module TrackingPolicy
	class Create
		include ActiveModel::Validations

		I18N_PATH = "errors.policies."

		attr_reader :tracking_number, :carrier, :results

		def initialize(tracking_number, carrier)
			@tracking_number = tracking_number
			@carrier = carrier
			@results = {}
			@error_types = []
		end

		validate :is_tracking_number_valid?
		validate :is_carrier_valid?
		validate :already_exists_tracking?, unless: Proc.new { |s| s.fail_by?(:invalid_carrier) || s.fail_by?(:invalid_tracking_number) } 

		def fail_by?(error_type)
			@error_types.include?(error_type)
		end

		private 

		def is_tracking_number_valid?
			factory = -> { TrackingNumberValidator.new(tracking_number) }
			execute_validation(factory, :invalid_tracking_number)
		end

		def is_carrier_valid?
			factory = -> { CarrierValidator.new(carrier) }
			execute_validation(factory, :invalid_carrier)
		end

		def already_exists_tracking?
			factory = -> { TrackingUniquenessValidator.new(tracking_number, carrier) }
			@results[:tracking] = execute_validation(factory, :already_exists)
		end

		def execute_validation(factory_validator, error_type)
			validator = factory_validator.call
			unless validator.valid?
				self.errors.merge!(validator.errors)
				@error_types << error_type
			end
			
			validator.result
		end
	end
end
		
