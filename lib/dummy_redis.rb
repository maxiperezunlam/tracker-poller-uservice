# frozen_string_literal: true

class DummyRedis
	attr_reader :messages

	def initialize
		@messages = {}
		@subscriptions = {}
	end

	def publish(channel_name, message)
		@messages[channel_name] ||= Queue.new
		@messages[channel_name].push(message)
	end
	
	def subscribe(channel_name)
		@subscriptions[channel_name] = true

		loop @subscriptions[channel_name] do	
			messages = @messages[channel_name]
			if messages.present?
				length = messages.length
				length.times do
					yield(channel_name, messages.pop)	
				end
			end
		end
	end

	def unsubscribe(channel_name)
		@subscribed[channel_name] = false
	end
end
