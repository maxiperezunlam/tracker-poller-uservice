# frozen_string_literal: true

module TrackerShip
	class All
		def self.load!
			Dir[File.join(__dir__, "**" , "*.rb")].each do |dep|
				require_relative dep
			end
		end
	end
end
