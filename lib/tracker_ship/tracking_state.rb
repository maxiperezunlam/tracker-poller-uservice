module TrackerShip
	module TrackingState
		# Se podria usar meta programacion para definirlos programaticamente a todos los estados
		# admisibles pero por simplicidad y claridad lo hacemos a mano.
		def self.valid_states
			[
				:created,
				:on_transit,
				:delivered,
				:exception
			]
		end
	end
end
