# frozen_string_literal: true

module TrackerShip
	module TrackingState
		class Base
			attr_reader :description

			def initialize(description = nil)
				@description = description
			end

			def exception?
				false
			end

			def delivered?
				false
			end
	
			def on_transit?
				false
			end
		
			def created?
				false
			end
			
			def error?
				false
			end

			def to_s
				self.class.formatted_s
			end

			def self.formatted_s
				raise NotImplementedError, "#{self.class} must implement '#{__method__}' method"
			end
		end
	end
end
