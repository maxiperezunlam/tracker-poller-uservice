# frozen_string_literal: true

module TrackerShip
	module TrackingState
		class Delivered < TrackerShip::TrackingState::Base
			def delivered?
				true
			end

			def self.formatted_s
				"DELIVERED"
			end
		end
	end
end
