# frozen_string_literal: true

module TrackerShip
	module TrackingState
		class Created < TrackerShip::TrackingState::Base
			def created?
				true
			end

			def self.formatted_s
				"created"
			end
		end
	end
end
