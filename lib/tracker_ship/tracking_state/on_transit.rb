# frozen_string_literal: true

module TrackerShip
	module TrackingState
		class OnTransit < TrackerShip::TrackingState::Base
			def on_transit?
				true
			end

			def self.formatted_s
				"ON_TRANSIT"
			end
		end
	end
end
