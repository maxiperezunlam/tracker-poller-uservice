# frozen_string_literal: true

module TrackerShip
	module TrackingState
		class MappingError < Base
			def mapping_error?
				true
			end

			def self.formatted_s
				"MAPPING_ERROR"
			end
		end
	end
end
