# frozen_string_literal: true

module TrackerShip
	module TrackingState
		class Exception < TrackerShip::TrackingState::Base
			def exception?
				true
			end

			def self.formatted_s
				"EXCEPTION"
			end
		end
	end
end
