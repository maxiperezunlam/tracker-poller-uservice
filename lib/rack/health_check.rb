# https://medium.com/@goncalvesjoao/how-to-have-a-working-health-endpoint-while-rails-is-500ring-509efc1da42c
module Rack
	class HealthCheck
		def call(env)
			status = {
				api: {
					connected: api_connected
				},
				redis: {
					connected: redis_connected
				},
				db: {
					connected: db_connected,
					migrations_updated: migrations_updated
				},
				sidekiq: {
					running: sidekiq_state
				}
			}

			return [200, {}, [status.to_json]]
		end
		
		protected

		# Esto podria modificar e incluso adquirir la direccion del endpoint desde un archivo externo.
		# Por simplicidad asumimos que ejecuta localmente y en el puerto por defecto.
		# No lo consultamos usando la API de Rails por que es justamente lo que estamos chequeando
		def api_connected
			begin
				response = nil
				Net::HTTP.start(ENV['TRACKER_POLLER_API_URL'], ENV['TRACKER_POLLER_API_PORT']) do |http|
					response = http.head('/v1/health')
				end
				response.present? && response.code.present? && response.code == '200'
			rescue
				false
			end
		end

		def redis_connected
			$redis.ping == "PONG" rescue false
		end

		def db_connected
			begin
				ApplicationRecord.establish_connection
				ApplicationRecord.connection
				ApplicationRecord.connected?
			rescue
				false
			end
		end

		def migrations_updated
			return false unless db_connected
			!ApplicationRecord.connection.migration_context.needs_migration?
		end

		def sidekiq_state
			`ps -aux | grep sidekiq | grep -v grep`.present?
		end
	end
end
