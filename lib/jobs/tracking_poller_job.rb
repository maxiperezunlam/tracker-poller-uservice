require 'sidekiq-scheduler'
require 'em-synchrony'
require 'dry-monads'
require 'json'
require 'fedex'
require 'dotenv'
require 'active_record'
require 'yaml'
require 'redis'

require_relative File.join(__dir__, '..', '..', 'config', 'initializers', 'redis')
require_relative File.join(__dir__, '..', 'tracker_ship', 'all')

#LOAD ALL ENVIRONMENT VARIABLES
Dotenv.load('./.env')

#LOAD ALL CLASSES
TrackerShip::All.load!

class TrackingPollerJob
	include Sidekiq::Worker

	DEFAULT_CARRIER = "fedex"
	DEFAULT_ENV = "development"
	VALID_STATES = TrackerShip::TrackingState.valid_states.map.with_index.to_h.freeze 

	attr_accessor :redis, :poller, :updated_at
	
	def perform
		begin
			configure_job!	
			update_delivery_statuses
		rescue StandardError => e
			@logger.error(e.message) if @logger.present?
		ensure #Nos aseguramos que en caso que haya una conexion de base de datos se limpien para evitar fuga de conexiones 
		       #En caso que no este ejecutando la api y el job ejecute en un server individual
			if ActiveRecord::Base.connected? && ENV["POLLER_JOB_ONLY_JOB_SERVER"].present? && ENV["POLLER_JOB_ONLY_JOB_SERVER"] == 'true'
				ActiveRecord::Base.connection.close
			end
		end	
	end
	
	private 

	def update_delivery_statuses
		statuses = @poller.call
		statuses.each do |data| 
			begin
				case data
				in TrackerShip::Status::Success
					update_status_on_db(data)
					redis.publish ENV["TRACKINGS_DELIVERY_STATUS_CHANNEL"], format_info(data)
				in TrackerShip::Status::Error
					error_message = "error with (tracking_number=#{data.tracking_number}, delivery_status=#{data.delivery_status})"
					@logger.error(error_message)
				end
			rescue => e
				@logger.error("============ Error with #{data.to_s} ================")
				@logger.error(e.message)	
			end	
		end
	end

	def configure_job!
		@logger = Logger.new(ENV["POLLER_JOB_LOG_PATH"] || STDOUT)
		set_updated_at_date!
		load_poller_data!
		configure_db!
		configure_fetcher!	
		configure_poller!
		configure_redis!
	end

	def set_updated_at_date!
		@updated_at ||= Time.now.utc
	end

	def load_poller_data!
		@carrier = ENV["POLLER_JOB_CARRIER"] 
		@poller_config = "TrackerShip::Config::#{@carrier.classify}TrackerPoller".constantize.new
	end

	def configure_db!
		db_file = File.open(File.join(__dir__, ENV["POLLER_JOB_DB_CONFIG_FILEPATH"]))
		db_configuration = YAML::load(db_file)[ENV["POLLER_JOB_ENV"]]
		
		# TODO ver valores acordes para que directamente la db soporte la carga conjunta de la api y sidekiq	
		db_configuration["pool"] = 32
		db_configuration["timeout"] = 15000

		unless ActiveRecord::Base.connected?	
			ActiveRecord::Base.establish_connection(
				db_configuration
			)
		end
	end

	#El fetcher esta acoplado a sqlite. Lo ideal seria incluir un microorm que resuelva las diferencias de sintaxis entre 
	#los dialectos de SQL. Aqui el problema son las funciones de fecha donde no estan estandarizadas en ANSI SQL. 
	def configure_fetcher!
		#Como nadie tiene acceso externo al codigo no existe riesgo de SQL INJECTION
		#A menos que se tenga acceso al servidor y se modifique el .env ( En ese caso se tendria un problema mayor :( )
		@fetcher = -> do
			puts "Executing Fetcher"
			sql = %{
				SELECT tracking_number FROM trackings
				WHERE type like '#{@carrier}%' AND updated_at >= datetime('now', '-3 month')
			}
			result = ActiveRecord::Base.connection.execute(sql)
			result.map do |record|
				record['tracking_number']
			end
		end
	end

	def configure_poller!
		@poller ||= "TrackerShip::TrackerPoller::#{@carrier.classify}".constantize.new @poller_config, @fetcher
	end

	def configure_redis!
		@redis ||= $redis
	end

	def format_info(info)
		delivery_status = map_delivery_status(info.delivery_status)
		description = get_description(info.delivery_status)

		if delivery_status.present?
			{
			   tracking_number: info.tracking_number, 
			   status: delivery_status, 
			   description: description,
			   updated_at: @updated_at	
			}.to_json
		end
	end

	def update_status_on_db(info)
		delivery_status = map_delivery_status(info.delivery_status)
		description = get_description(info.delivery_status)

		if delivery_status.present?
			update_sql = %{
				UPDATE trackings 
				SET status = #{delivery_status}, 
				    description = '#{description}', 
				    updated_at = '#{@updated_at.to_s}'
				WHERE tracking_number like '#{info.tracking_number}'
				AND type like '#{@carrier}%' 
			}
			ActiveRecord::Base.connection.execute(update_sql)
		else
			raise RuntimeError, "The delivery status is nil"
		end
	end

	def get_description(delivery_status)
		value = nil
		if delivery_status.is_a?(Dry::Monads::Result::Success)
			value = delivery_status.value!
		elsif delivery_status.is_a?(Dry::Monads::Result::Failure)
			value = delivery_status.failure
		end
		
		return value.description if value.present? && value.respond_to?(:description)
		
		return ""
	end
	
	def map_delivery_status(delivery_status)
		value = nil
		if delivery_status.is_a?(Dry::Monads::Result::Success)
			value = delivery_status.value!
		elsif delivery_status.is_a?(Dry::Monads::Result::Failure)
			value = delivery_status.failure
		end
		
		if value.present?
			key = value.class.formatted_s
			VALID_STATES[key.downcase.to_sym]
		else
			nil
		end	
	end
end
