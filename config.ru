# This file is used by Rack-based servers to start the application.

require_relative "config/environment"
require_relative "lib/rack/health_check"

run Rails.application
Rails.application.load_server

map '/health' do
	run Rack::HealthCheck.new
end
